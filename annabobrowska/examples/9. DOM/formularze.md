
```js
paramsFormElem
// <form id=​"paramsFormElem">​…​</form>​
paramsFormElem.elements
// HTMLFormControlsCollection(3) 
  // total: input#total.form-control, 
  // interest: input#interest.form-control, 
  // periods: input#periods.form-control]
  // 0: input#total.form-control
  // 1: input#interest.form-control
  // 2: input#periods.form-controli
paramsFormElem.elements.total.value 
'1000'
paramsFormElem.elements.total.value = 123
123

fd = new FormData(paramsFormElem)

Object.fromEntries(fd.entries())
// {total: '123', interest: '15', periods: '12'} 

