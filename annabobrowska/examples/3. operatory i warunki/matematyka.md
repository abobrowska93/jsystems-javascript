
```js

2 + 2 * 2
// 6
2 + (2 * 2)
// 6
(2 + 2) * 2
// 8

// Hexadecimal
(255).toString(16)
// 'ff'

// Binary

(12).toString()
// '12'
(12).toString(10)
// '12'
(12).toString(2)
// '1100'
(1).toString(2)
// '1'
(3).toString(2)
// '11'
((~ 1) + 1).toString(2)
// '-1'
parseInt('1010',2) & 1000
// 8

```