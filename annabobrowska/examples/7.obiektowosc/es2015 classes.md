```js

class Person{
    constructor(name){
        this.name = name;
    }
    
    sayHello(){
        return `My name is ${this.name}`
    }
    
    toString(){
        return this.sayHello()
    }   

}
class Employee extends Person{
    constructor(name){
        super(name) // Person.constructor.apply(this,[name])
    }
    
    sayHello(){
        return `I am employee and ${super.sayHello()}`
    }

}

alice = new Person('Alice')
tom = new Employee('Tom')

// Employee {name: 'Tom'}
//     name: "Tom"
//     [[Prototype]]: Person
//         constructor: class Employee
//         sayHello: ƒ sayHello()
//             [[Prototype]]: 
//                 constructor: class Person
//                 sayHello: ƒ sayHello()
//                 toString: ƒ toString()
//                 [[Prototype]]: Object

alice.sayHello()
// 'My name is Alice'
tom.sayHello()
// 'I am employee and My name is Tom'
'Welcome ' + alice + ' ' + tom;
// 'Welcome My name is Alice I am employee and My name is Tom'

tom instanceof Employee
// true

tom instanceof Person
// true
```

## Class and lambda 'this' binding

```js
class Employee extends Person{
    constructor(name){
        super(name) // Person.constructor.apply(this,[name])
    }
    
    sayHello = () => {
        return `I am employee and ${super.sayHello()}`
    }

}

alice = new Person('Alice')
tom = new Employee('Tom')

// Employee {name: 'Tom', sayHello: ƒ}

funkcja = tom.sayHello

funkcja()
// 'I am employee and My name is Tom'
```