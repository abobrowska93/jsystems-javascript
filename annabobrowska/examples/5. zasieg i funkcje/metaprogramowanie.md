
```js
eval(' x = 1; ')
1
x 
1
function abc(x,y,z){ require('jquery') }
// undefined
abc
// ƒ abc(x,y,z){ require('jquery') }
abc.name 
// 'abc'
abc.length
// 3
abc.toString()
// "function abc(x,y,z){ require('jquery') }"
abc.toString().match(/require\(\'(.*?)\'\)/)

const sum = new Function('a', 'b', 'return a + b');
console.log(sum(2, 6));
// VM50316:3 8

// IF you want to get hacked: ;-)
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/eval#never_use_eval!
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function/Function
```