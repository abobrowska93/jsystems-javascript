# forEach

```js
lista = [1, 2, 3, 4, 5];
// (5) [1, 2, 3, 4, 5]
lista2 = [];

lista.forEach(function (item, index, all) {
  console.log(item, index, all);
  lista2.push(item * 2);
});
```

# Every

```js
lista = [1, 2, 3, 4, 5];

r = lista.every(function (item, index, all) {
  console.log(item, index, all);
  return item % 2 !== 0;
});
r;
// VM59314:4 1 0 (5) [1, 2, 3, 4, 5]
// VM59314:4 2 1 (5) [1, 2, 3, 4, 5]
// false
```

# Some

```js
r = lista.some(function (item, index, all) {
  console.log(item, index, all);
  return item % 2 == 0;
});
r;
// VM59421:4 1 0 (5) [1, 2, 3, 4, 5]
// VM59421:4 2 1 (5) [1, 2, 3, 4, 5]
// true
```

# Polymorfism / Substition

```js
lista = [1, 2, 3, 4, 5];

callbackFn = function (item, index, all) {
  console.log(item, index, all);
  return item % 2 == 0;
};

r = lista.some(callbackFn);
r;
```

# Filter

```js
lista = [1,2,3,4,5]

wynik = lista.filter(function(item, index, all) { 
    console.log(item,index,all)
    return item % 2 == 0
});
wynik
// VM59755:4 1 0 (5) [1, 2, 3, 4, 5]
// VM59755:4 2 1 (5) [1, 2, 3, 4, 5]
// VM59755:4 3 2 (5) [1, 2, 3, 4, 5]
// VM59755:4 4 3 (5) [1, 2, 3, 4, 5]
// VM59755:4 5 4 (5) [1, 2, 3, 4, 5]
// (2) [2, 4]
lista
// (5) [1, 2, 3, 4, 5]
```

# Map

```js
lista = [1,2,3,4,5]

wynik = lista.map(function(item, index, all) { 
    //console.log(item,index,all)
    return item * 2
});
wynik
// (5) [2, 4, 6, 8, 10]
lista.length == wynik.length
// true
```

# Reduce

```js
lista = [1,2,3,4,5]

wynik = lista.reduce(function(acc, item, index, all) { 
    console.log(acc, item, acc+ item)
    return acc + item
}, 0);
wynik
// VM60863:4 0 1 1
// VM60863:4 1 2 3
// VM60863:4 3 3 6
// VM60863:4 6 4 10
// VM60863:4 10 5 15
// 15
```