```js
var i = 5;

var array = [1,2,3];
for(var i in array ) {
    console.log( i, array[i] )
}

console.log(i)
// VM24337:5 0 1
// VM24337:5 1 2
// VM24337:5 2 3
// VM24337:8 2
// undefined
x 
// VM24365:1 Uncaught ReferenceError: x is not defined
//     at <anonymous>:1:1
// (anonymous) @ VM24365:1
if(false){ 
    var x = 2 
}
console.log( x )
// VM24523:4 undefined

var y = 2;

function funkcja(){
    var y = 3
}
funkcja()

console.log(y) 
// VM24743:8 2
```