```js
HTMLDivElement
// ƒ HTMLDivElement() { [native code] }

new HTMLDivElement()
// VM2817:1 Uncaught TypeError: Illegal constructor
//     at <anonymous>:1:1
// (anonymous) @ VM2817:1

div = document.createElement('div')
// <div>​</div>​

div.parentElement
// null

div.id = 'test'
div.className = 'klasa'
div.style = 'color:red'
div.style.border = '1px solid red'
div.innerText = 'Ala ma kota'

div 
// <div id=​"test" class=​"klasa" style=​"color:​ red;​ border:​ 1px solid red;​">​Ala ma kota​</div>​

document.body.append(div) 


p = document.createElement('p')
p.innerText = 'test'
div.append(p)

p.className = 'card p-3 m-3'

```