
```js

$0.className += ' ' + 'show'
// 'collapse show'

$0.className = $0.className.replace(' show','')
// 'collapse'

$0.classList.add('show')
$0.classList.remove('show')

$0.classList.toggle('show')
// true

$0.classList.toggle('show')
// false

```