## Function constructor

```js
function Person(name) {
  return {
    name: name,
    sayHello() {
      return `Hello, Iam ${name}`;
    },
  };
}

alice = Person("Alice");
bob = Person("Bob");

alice.sayHello();
("Hello, Iam Alice");
bob.sayHello();
("Hello, Iam Bob");

// Closure
bob.name = "Kate";
("Kate");
bob.sayHello();
("Hello, Iam Bob"); // Still BOB!
bob.name;
("Kate");
```

# Reference to instance (self/this)

```js
function Person(name) {
  var self = {
    name: name,
    sayHello() {
      return `Hello, Iam ${self.name}`;
    },
  };
  return self;
}

alice = Person("Alice");
bob = Person("Bob");

bob.name = "Kate";
bob.sayHello();
("Hello, Iam Kate");
```

# call, apply, new + this

```js
function Person(name) {
  this.name = name;
  this.sayHello = function () {
    return `Hello, Iam ${this.name}`;
  };
  return this;
}

alice = Person("Alice");
bob = Person("Bob");

bob.sayHello();
("Hello, Iam Bob");
alice.sayHello();
("Hello, Iam Bob");
window.name;
("Bob");
window.sayHello; // function..

alice = Person.call({ v: "callFn" }, "Alice");
bob = Person.apply({ v: "applyFn" }, ["Bob"]);
// bob = Person.apply(alice, ['Bob'])

// {v: 'applyFn', name: 'Bob', sayHello: ƒ}
bob.sayHello();
// 'Hello, Iam Bob'
alice.sayHello();
// 'Hello, Iam Alice'
kate = new Person("Kate");
// Person {name: 'Kate', sayHello: ƒ}
kate.sayHello();
("Hello, Iam Kate");
```

# Auto construct

```js

function autoContructor(){
    // ....
    if(this === window){
        return new autoContructor()
    }
}

y = autoContructor()
x = autoContructor()
autoContructor {}
```
