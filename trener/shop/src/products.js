// ========================
export const products = [
  { id: '123', name: 'Placki', category: 'placki', price: 9750, discount: 0.33, promoted: true },
  { id: '234', name: 'Naleśniki', category: 'nalesniki', price: 5650, discount: 0, promoted: false },
  { id: '345', name: 'Pączki', category: 'ciastka', price: 3050, discount: 0.2, promoted: true },
];

export default products