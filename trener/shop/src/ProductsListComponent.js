export class ProductsListComponent {
    isPromoted = false;
    limit = 2;
    tax = 23;

    /** @type {SloganGenerator} */
    sloganGenerator = null;

    /** @type {typeof ProductListItem} */
    renderItem = null;

    constructor(products) {
        this._products = products;
    }
    render() {
        if (this._products.length) {
            this.removeProductsFromList();
        }

        let count = 0;
        for (let product of this._products) {
            if (this.isPromoted !== undefined && !(product.promoted === this.isPromoted))
                continue;
            if (count++ == this.limit)
                break;

            if (this.sloganGenerator)
                var slogan = this.sloganGenerator?.generateSlogan(product);

            const prices = this.calculateProductPrices(product);
            const extra = { prices, slogan, count };

            const renderer = new this.renderItem(product, extra);
            const div = renderer.render();
            productsList.append(div);
        }
    }
    calculateProductPrices(product) {
        const netto = product.price - (product.price * product.discount);
        const brutto = netto * (1 + (this.tax / 100));
        return { /* netto:netto */ netto, brutto, currency: 'zł' };
    }
    removeProductsFromList() {
        while (productsList.firstElementChild) {
            productsList.firstElementChild.remove();
        }
    }
}
